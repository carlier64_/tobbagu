﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tobbagu1
{
    class Program
    {

        static void Main(string[] args)
        {

           const int add = 1, sub = 2, mul = 3, div = 4;
            int a = 0, b = 0, op = 0;
   
            Console.WriteLine("Add meg az elso szamot!");
            a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Add meg az masodik szamot!");
            b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Muveletek: ");
            Console.WriteLine("1. Összeadas");
            Console.WriteLine("2. Kivonas ");
            Console.WriteLine("3. Szorzas ");
            Console.WriteLine("4. Osztas ");

            op = Convert.ToInt32(Console.ReadLine());

            switch (op)
            {
                case add: Console.WriteLine($"Eredmeny {a + b}"); break;
                case sub: Console.WriteLine($"Eredmeny {a - b}"); break;
                case mul: Console.WriteLine($"Eredmeny {a * b}"); break;
                case div: Console.WriteLine($"Eredmeny {a / b}"); break;
                default: Console.WriteLine("Nincs ilyen muvelet"); break; 
            }


            Console.ReadKey();

        }
    }
}
